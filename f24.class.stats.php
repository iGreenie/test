<?php

if( !class_exists( 'f24_Statistics' ) ) {

	class f24_Statistics {

		private static $instance;

		public static function init() {
			return self::$instance;
		}

		public function __construct() {
			add_action('admin_menu', array( $this, 'setup_adminmenu' ) );
		}

		// Funkce, která přidá nové menu do administrace
		function setup_adminmenu() {
		  	add_menu_page('Statistika', 'Statistika', 'edit_private_posts', 'forum24-statistics', array ($this, 'view_ctenost_clanku'), 'dashicons-chart-bar', 5);
		  	add_submenu_page('forum24-statistics', 'Čtenost článků', 'Čtenost článků', 'edit_private_posts', 'forum24-statistics', array ($this, 'view_ctenost_clanku'));
		  	add_submenu_page('forum24-statistics', 'Autorské články', 'Autorské články', 'edit_private_posts', 'forum24-statistics-authors', array ($this, 'view_autorskeclanky'));
		}

		// Preparace hlavičky
		function get_header($title) {
			echo '<link rel="stylesheet" href="https://www.w3schools.com/lib/w3.css">';
			echo '<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>';
			echo '<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>';
			echo '<script type="text/javascript" src="http://forum24.cz/wp-content/themes/forum24/functions/stats/sorter/jquery.tablesorter.min.js"></script>';
			echo '<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />';
			echo '<style>a { text-decoration: none; } .daterangepicker { display: none; }</style>';
			echo '<style>@media print { #adminmenumain { display: none; } #wpcontent { margin-left: 0; } .update-nag { display: none } }</style>';

			echo '<div class="wrap">';
				echo '<h3 style="float: right;"><input type="text" name="daterange" value="měsíc"></h3>'; // Vyhledávání
				echo '<h2>'.$title.'</h2>';
		}

		// Preparace patičky
		function get_footer() {
		?>
		<script>
			jQuery('input[name="daterange"]').daterangepicker({
			    "ranges": {
			        "Dnes": [
			            moment(),
			            moment()
			        ],
			        "Včera": [
			            moment().subtract(1, 'days'),
			            moment().subtract(1, 'days')
			        ],
			        "Posledních 7 dní": [
			            moment().subtract(6, 'days'),
			            moment()
			        ],
			        "Tento měsíc": [
			            moment().startOf('month'),
			            moment().endOf('month')
			        ],
			        "Minulý měsíc": [
			            moment().subtract(1, 'month').startOf('month'),
			            moment().subtract(1, 'month').endOf('month')
			        ]
			    },
			    "locale": {
			        "format": "DD.MM.YYYY",
			        "separator": " - ",
			        "applyLabel": "Nastavit",
			        "cancelLabel": "Zrušit",
			        "fromLabel": "Od",
			        "toLabel": "Do",
			        "customRangeLabel": "Vlastní",
			        "weekLabel": "T",
			        "daysOfWeek": [
			            "Ne",
			            "Po",
			            "Út",
			            "St",
			            "Čt",
			            "Pá",
			            "So"
			        ],
			        "monthNames": [
			            "Leden",
			            "Únor",
			            "Březen",
			            "Duben",
			            "Květen",
			            "Červen",
			            "Červenec",
			            "Srpen",
			            "Zárí",
			            "Říjen",
			            "Listopad",
			            "Prosinec"
			        ],
			        "firstDay": 1
			    },
			    <?php if($_GET['od']) : ?>
				    "startDate": moment("<?php echo $_GET['od']; ?>"),
				    "endDate": moment("<?php echo $_GET['do']; ?>"),
			    <?php else : ?>
			    	"startDate": moment().startOf('month'),
			    	"endDate": moment().endOf('month'),
			    <?php endif; ?>
			    "opens": "center"
			}, function(start, end, label) {
			  console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
			  window.location.href = "http://" + window.location.hostname + window.location.pathname + window.location.search + "&od=" + start.format('YYYY-MM-DD') + '&do=' + end.format('YYYY-MM-DD');
			});
		</script>
		</div>
	<?php 
	}

		// Funkce na získání vybraných příspěvků
		function get_posts() {
			$today = getdate();

			if($_GET['od']) :

				$year  = substr($_GET['do'], 0, 4);
				$month = substr($_GET['do'], 5, 2);
				$day   = substr($_GET['do'], 8, 2);

				$date = array(
					'after'  => $_GET['od'],
					'before'    => array(
						'year'  => $year,
						'month' => $month,
						'day'   => $day,
					),
					'inclusive' => true,
				);

			else :

				$date = array(
					'year' => $today['year'],
					'month' => $today['mon'],
				);

			endif;

			if ( $_GET['author'] ) {
				$author_query = array(
					array(
						'taxonomy' => 'autor',
						'field'    => 'slug',
						'terms'    => $_GET['author'],
					),
				);
			} else {
				$author_query = array();
			}

			$posts = new WP_Query ( 
				array(
					
					'post_type' => array( 'post', 'glosy', 'online' ),

					'ignore_sticky_posts' => true,
					'posts_per_page' => -1,
					'meta_key'       => '_count-views_all',

					// order by views
					'orderby'        => 'meta_value_num',
					'order'          => 'DESC',

					// order by date
					//'orderby'        => 'date',
					//'order'          => 'ASC',

					'date_query'	=> $date,

					'tax_query' 	=> $author_query,
					
				)
			);

			return $posts->posts;
		}

		// Funkce na získání příspěvků podle autora
		function get_author_posts($author, $return = "total") {
			$today = getdate();

			if($_GET['od']) :

				$year  = substr($_GET['do'], 0, 4);
				$month = substr($_GET['do'], 5, 2);
				$day   = substr($_GET['do'], 8, 2);

				$date = array(
					'after'  => $_GET['od'],
					'before'    => array(
						'year'  => $year,
						'month' => $month,
						'day'   => $day,
					),
					'inclusive' => true,
				);

			else :

				$date = array(
		            'year' => $today['year'],
					'month' => $today['mon'],
				);

			endif;

			$posts = new WP_Query ( 
				array(
					
					//Type & Status Parameters
					'post_type' => array( 'post', 'glosy', 'online' ),
					
					'post_status' => array(
						'publish',
					),
					
					//Order & Orderby Parameters
					'ignore_sticky_posts' => false,
					
					'date_query' => $date,
					
					//Pagination Parameters
					'posts_per_page'         => -1,

					'tax_query' => array(
						array(
							'taxonomy' => 'autor',
							'field'    => 'slug',
							'terms'    => $author->slug,
						),
					),

		            'fields' => 'ids',
					
				)
			);

		    if($return == "total")  
			   return $posts->found_posts;
		    else if($return == "ids")
		        return $posts->posts;
		    else
		        return $posts->found_posts;
		}

		// Funkce sečte čtenost všech článků od jednoho autora
		function count_post_views($author) {
		    $count = array();
		    $posts = $this->get_author_posts($author, "ids");

		    foreach ($posts as $post) {
		        $shortcode = '[post_view id="'.$post.'"]';
		        $shortcode = strip_tags(do_shortcode($shortcode));
		        $count[] = (int) $shortcode;
		    }

		    $sum = array_sum($count);
		    return $sum;
		}

		// Funkce na setřízení autorů
		function sort_authors($field) {
			$result = array();
			$authors = get_field($field, 'option');

			foreach ($authors as $author) {
				$total = $this->get_author_posts($author, "total");
		        $views = $this->count_post_views($author);

		        if($total == 0) continue;

				$array[$author->name] = array( 'slug' => $author->slug, 'total' => $total, 'views' => $views );
			}
			arsort($array);
			return $array;
		}

		// Setřídí jména podle příjmení
		function lastNameSort($a, $b) {
		    $aLast = end(explode(' ', $a));
		    $bLast = end(explode(' ', $b));

		    return strcasecmp($aLast, $bLast);
		}

		// Čtenost článků
		function view_ctenost_clanku() {
			$this->get_header('Čtenost článků');
?>
			<div class="notice notice-info" style="margin-top: 20px;">
		        <p>Tabulky lze setřídit kliknutím na hlavičku (např. Autor). <br/> Kliknutím na název článku otevřete článek v novém okně a kliknutím na jméno zobrazíte všechny autorské články autora za dané období.</p>
		    </div> 

		    <input type="text" id="SearchArticleInput" onkeyup="searchArticle()" placeholder="Vyhledat článek...">

	    	<table id="postViews" class="w3-table-all tablesorter">
				<thead>
					<tr>
						<th>Datum</th>
						<th>Autor</th>
						<th>Název článku</th>
						<th>Čtenost</th>
					</tr>
				</thead>
				<tbody>

			<?php

				$posts = $this->get_posts();

				$url = $_SERVER['REQUEST_URI'];

			    if($_GET['od']) {
			        $string = 'admin.php?page=forum24-statistics?od='.$_GET["od"].'&do='.$_GET["do"];
			        $url = str_replace($url, 'admin.php?page=forum24-statistics', $string);
			        $prefix = "&author=";
			    } else {
			        $prefix = "?author=";
			    }

				foreach ($posts as $post) {

					$shortcode = '[post_view id="'.$post->ID.'"]';
					$shortcode = strip_tags(do_shortcode($shortcode));
					$views = (int) $shortcode;

					$authorid = $post->post_author;
					$custom_author = get_post_meta($post->ID, 'mz_fcm_author_name', true );
					$authorObject    = wp_get_post_terms( $post->ID, 'autor', array("fields" => "names") );

					if($custom_author) $author = $custom_author;
					elseif($authorObject) $author = $authorObject;
					else $author = get_the_author_meta('display_name', $post->post_author);

					echo '<tr>';
						echo '<td>'.get_the_date('j.n', $post->ID).'</td>';
						if(is_array($author)) {
							foreach ($author as $author) {
								$slug = remove_accents($author);
								$slug = str_replace(" ", "-", $slug);
								echo '<td><a href="'.add_query_arg( 'author', $slug, $url ).'" target="_blank">'.$author.'</a></td>';
								break;
							}
						} else {
							$slug = remove_accents($author);
							$slug = str_replace(" ", "-", $slug);
							echo '<td><a href="'.add_query_arg( 'author', $slug, $url ).'" target="_blank">'.$author.'</a></td>';
						}
						echo '<td><a href="'.get_post_permalink( $post->ID ).'" target="_blank">'.$post->post_title.'</a></td>';
						echo '<td>'.$views.'</td>';
					echo '</tr>';
				}

			?>
				</tbody>
			</table>
			<style>
				#SearchArticleInput {
				    background-image: url('https://www.w3schools.com/css/searchicon.png'); /* Add a search icon to input */
				    background-position: 10px 12px; /* Position the search icon */
				    background-repeat: no-repeat; /* Do not repeat the icon image */
				    width: 100%; /* Full-width */
				    font-size: 16px; /* Increase font-size */
				    padding: 12px 20px 12px 40px; /* Add some padding */
				    border: 1px solid #ddd; /* Add a grey border */
				    margin-bottom: 12px; /* Add some space below the input */
				}
			</style>
			<script>
				jQuery(document).ready(function() 
				    { 
				        jQuery("#postViews").tablesorter({ 
					        // sort on the first column and third column, order asc 
					        sortList: [[3,1]] 
					    }); 
				    } 
				); 


				function searchArticle() {
				  // Declare variables 
				  var input, filter, table, tr, td, i;
				  input = document.getElementById("SearchArticleInput");
				  filter = input.value.toUpperCase();
				  table = document.getElementById("postViews");
				  tr = table.getElementsByTagName("tr");

				  // Loop through all table rows, and hide those who don't match the search query
				  for (i = 0; i < tr.length; i++) {
				    td = tr[i].getElementsByTagName("td")[2];
				    if (td) {
				      if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
				        tr[i].style.display = "";
				      } else {
				        tr[i].style.display = "none";
				      }
				    } 
				  }
				}
			</script>
<?php
			$this->get_footer();
		}

		// Autorské články
		function view_autorskeclanky() {
			$this->get_header('Autorské články');
?>
		<div class="notice notice-info" style="margin-top: 20px;">
	        <p>Tabulky lze setřídit kliknutím na hlavičku (např. Autor). Kliknutím na jméno zobrazíte všechny autorské články autora za dané období.<br/> Pokud v tabulce chybí autor, tak za dané období nenapsal žádný článek.</p>
	    </div> 

		<input type="text" id="SearchAuthorInput" onkeyup="searchAuthor()" placeholder="Vyhledat autora...">

		<table id="authorPostsTable" class="w3-table-all tablesorter">
	        <thead>
	    		<tr>
	    			<th>Autor</th>
	    			<th>Počet článků</th>
	                <th>Celková čtenost</th>
	    		</tr>
	        </thead>
	        <tbody>

	<?php

		$authors = $this->sort_authors('f24_statistics_people');

	    $url = $_SERVER['REQUEST_URI'];
	    $url = str_replace($url, 'admin.php?page=forum24-statistics-authors', 'admin.php?page=forum24-statistics');

	    if($_GET['od']) {
	        $string = 'admin.php?page=forum24-statistics&od='.$_GET["od"].'&do='.$_GET["do"];
	        $url = str_replace($url, 'admin.php?page=forum24-statistics', $string);
	        $prefix = "&author=";
	    } else {
	        $prefix = "?author=";
	    }


		foreach ($authors as $author => $post) {
			echo '<tr>';
				echo '<td><a href="'.add_query_arg( 'author', $post['slug'], $url ).'" target="_blank">'.$author.'</a></td>';
				echo '<td>'.$post['total'].'</td>';
	            echo '<td>'.$post['views'].'</td>';
			echo '</tr>';
		}

	?>

	        </tbody>
	    </table>

	    <style>
			#SearchAuthorInput {
		    background-image: url('https://www.w3schools.com/css/searchicon.png'); /* Add a search icon to input */
		    background-position: 10px 12px; /* Position the search icon */
		    background-repeat: no-repeat; /* Do not repeat the icon image */
		    width: 100%; /* Full-width */
		    font-size: 16px; /* Increase font-size */
		    padding: 12px 20px 12px 40px; /* Add some padding */
		    border: 1px solid #ddd; /* Add a grey border */
		    margin-bottom: 12px; /* Add some space below the input */
		}
		</style>
		<script>
		jQuery(document).ready(function() 
		    { 
		        jQuery("#authorPostsTable").tablesorter( {sortList: [[1,1]]} ); 
		    } 
		); 

		function searchAuthor() {
		  // Declare variables 
		  var input, filter, table, tr, td, i;
		  input = document.getElementById("SearchAuthorInput");
		  filter = input.value.toUpperCase();
		  table = document.getElementById("authorPostsTable");
		  tr = table.getElementsByTagName("tr");

		  // Loop through all table rows, and hide those who don't match the search query
		  for (i = 0; i < tr.length; i++) {
		    td = tr[i].getElementsByTagName("td")[0];
		    if (td) {
		      if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
		        tr[i].style.display = "";
		      } else {
		        tr[i].style.display = "none";
		      }
		    } 
		  }
		}

		jQuery('input[name="daterange"]').daterangepicker({
		    "ranges": {
		        "Dnes": [
		            moment(),
		            moment()
		        ],
		        "Včera": [
		            moment().subtract(1, 'days'),
		            moment().subtract(1, 'days')
		        ],
		        "Posledních 7 dní": [
		            moment().subtract(6, 'days'),
		            moment()
		        ],
		        "Tento měsíc": [
		            moment().startOf('month'),
		            moment().endOf('month')
		        ],
		        "Minulý měsíc": [
		            moment().subtract(1, 'month').startOf('month'),
		            moment().subtract(1, 'month').endOf('month')
		        ]
		    },
		    "locale": {
		        "format": "DD.MM.YYYY",
		        "separator": " - ",
		        "applyLabel": "Nastavit",
		        "cancelLabel": "Zrušit",
		        "fromLabel": "Od",
		        "toLabel": "Do",
		        "customRangeLabel": "Vlastní",
		        "weekLabel": "T",
		        "daysOfWeek": [
		            "Ne",
		            "Po",
		            "Út",
		            "St",
		            "Čt",
		            "Pá",
		            "So"
		        ],
		        "monthNames": [
		            "Leden",
		            "Únor",
		            "Březen",
		            "Duben",
		            "Květen",
		            "Červen",
		            "Červenec",
		            "Srpen",
		            "Zárí",
		            "Říjen",
		            "Listopad",
		            "Prosinec"
		        ],
		        "firstDay": 1
		    },
		    <?php if($_GET['od']) : ?>
			    "startDate": moment("<?php echo $_GET['od']; ?>"),
			    "endDate": moment("<?php echo $_GET['do']; ?>"),
		    <?php else : ?>
		    	"startDate": moment().startOf('month'),
		    	"endDate": moment().endOf('month'),
		    <?php endif; ?>
		    "opens": "center"
		}, function(start, end, label) {
		  console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
		  window.location.href = "http://" + window.location.hostname + window.location.pathname + "admin.php" + window.location.search + "&od=" + start.format('YYYY-MM-DD') + '&do=' + end.format('YYYY-MM-DD');
		});
		console.log(moment().startOf('month'));
		</script>
	
<?php
			$this->get_footer();
		}

	}

}

$f24_stats = new f24_Statistics();
$f24_stats->init();
