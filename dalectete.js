// JS funkce pro zobrazování určitého elementu (součást souboru f24.class.dalectete.php)

(function($) {
	var media = wp.media, shortcode_string = "ctete";
	wp.mce = wp.mce || {};
	wp.mce.dalectete = {
		shortcode_data: {},
		template: media.template( 'editor-dalectete' ),
		getContent: function() {
			var options = this.shortcode.attrs.named;
			options.postid = this.shortcode.content;
			console.log(this.shortcode);
			return this.template(options);
		},
		replaceContent: function(data) {
			return this.template(data);
		},
		initialize: function() {
			var self = this;

			var options = this.shortcode.attrs.named;
			options.postid = this.shortcode.content;
			
			var data = {
				action: 'dalectete',
				postid: options.postid
			};

			jQuery.post(ajaxurl, data, function(response) {
				var options 	  = self.shortcode.attrs.named;
				options.title 	  = response.title;
				options.image 	  = response.image;
				options.excerpt   = response.excerpt;
				options.permalink = response.permalink;
				var content = self.getContent();
				self.setContent(content);
			});
		},
		edit: function( data ) {
			var self = this;
			var shortcode_data = wp.shortcode.next(shortcode_string, data);
			var values = shortcode_data.shortcode.attrs.named;
			values.content = shortcode_data.shortcode.content;
			self.popupWindow(tinyMCE.activeEditor, values);
		},
		popupWindow: function(editor, values, onsubmit_callback){
			values = values || [];
			var self = this;
			if(typeof onsubmit_callback !== 'function'){
				onsubmit_callback = function( e ) {
					// Insert content when the window form is submitted (this also replaces during edit, handy!)
					
					var data = {
						action: 'dalectete',
						postid: e.data.postid
					};

					jQuery.post(ajaxurl, data, function(response) {

						var attrsset = {
							postid: response.postid,
							title: response.title,
							image: response.image,
							excerpt: response.excerpt,
							permalink: response.permalink,
						};

						var args = {
							tag     : shortcode_string,
							type    : 'closed',
							content : e.data.postid,
							attrs: attrsset
						};

						console.log(args);

						editor.insertContent( wp.shortcode.string( args ) );
					});

				};
			}
			editor.windowManager.open( {
				title: 'Dále čtěte',
				body: [
					{
						type: 'textbox',
						name: 'postid',
						label: 'ID článku',
						value: values.content
					}
				],
				onsubmit: onsubmit_callback
			} );
		}
	};
	wp.mce.views.register( shortcode_string, wp.mce.dalectete );
}(jQuery));

jQuery(document).ready( function() {

	tinymce.PluginManager.add( 'f24_dalectete_tinymce', function( editor ) {

		editor.addButton( 'ctete', {
			text: 'Dále čtěte',
			icon: false,
			onclick: function() {
				wp.mce.dalectete.popupWindow(editor);
			}
		});

	});


});
¨
