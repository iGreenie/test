<?php

if( !class_exists( 'f24_shortcode_dalectete' ) ) {

	class f24_shortcode_dalectete {

		private static $instance;

		public static function init() {
			return self::$instance;
		}

		public function __construct() {
			add_action( 'wp_ajax_dalectete', array( $this, 'f24_dalectete_get_post' ) );
			add_action( 'print_media_templates', array( $this, 'print_media_templates' ) );
			add_action( 'admin_head', array( $this, 'mce_button' ) );
		}

		public function mce_button() {
			// kontrola práv uživatele
			if ( !current_user_can( 'edit_posts' ) && !current_user_can( 'edit_pages' ) ) {
				return;
			}
			// kontrola WYSIWYG editoru
			if ( 'true' == get_user_option( 'rich_editing' ) ) {
				add_filter( 'mce_external_plugins', array( $this, 'add_mce_plugin' ) );
			}
		} 

		// Přidá MCE plugin
		public function add_mce_plugin( $plugin_array ) {
			$plugin_array['f24_dalectete_tinymce'] = 'dalectete.js';
			return $plugin_array;
		}

		// Zaregistruje v editoru tlačítko
		public function register_mce_button( $buttons ) {
			array_push( $buttons, 'dalectete' );
			return $buttons;
		}

		// Přidá tlačítko do výběru v editoru
		public function add_mce_button_to_editor( $in ) {
			$in['toolbar4'] = 'dalectete';
			return $in;
		}

		// object WP_POST vybraného článku pomocí AJAX
		public function f24_dalectete_get_post() {
			
			$postid = $_POST['postid'];
			$post = new stdClass();

			$post->postid 		= $postid;
			$post->title 		= get_the_title($postid);
			$post->permalink 	= get_permalink($postid);
			$post->image 		= get_the_post_thumbnail_url( $postid, 'onecol' );
			$post->excerpt 		= f24_return_excerpt_by_char(90, $postid);

			wp_send_json($post);
		}

		// Frontend shortcode
		function register_shortcode($atts, $content = null) {

			$permalink 	= get_permalink( $content );
			$title		= get_the_title( $content );
			$image		= get_the_post_thumbnail( $postid, 'onecol' );

		ob_start(); ?>

			<div class="prolink-big">
				<span>Dále čtěte</span>
				<?php echo $image; ?>
				<h2><a href="<?php echo $permalink ?>" target="_blank" onClick="ga('send', 'event', 'Odkaz', 'Prolink', '<?php echo $title; ?>');"><?php echo $title; ?></a></h2>
				<p><?php f24_get_excerpt_by_char(90, $content); ?>...</p>
				<a href="<?php echo $permalink ?>" class="read-more" target="_blank" onClick="ga('send', 'event', 'Odkaz', 'Prolink', '<?php echo $title; ?>');">Číst článek &raquo;</a>
			</div>

		<?php
			
			return ob_get_clean();
		}

		// Zobrazení shortcodu v editoru
		public function print_media_templates() {
			if ( ! isset( get_current_screen()->id ) || get_current_screen()->base != 'post' )
            	return;
        ?>
        <script type="text/html" id="tmpl-editor-dalectete">
        	<div class="prolink-big">
				<span>Dále čtěte</span>
				<img src="{{data.image}}" />
				<div class="prolink-big-inner">
					<h2><a href="{{ data.permalink }}" target="_blank" onClick="ga('send', 'event', 'Odkaz', 'Prolink', '{{ data.title }}');">{{ data.title }}</a></h2>
					<p>{{ data.excerpt }}...</p>
					<a href="{{ data.permalink }}" class="read-more" target="_blank" onClick="ga('send', 'event', 'Odkaz', 'Prolink', '{{ data.title }}');">Číst článek &raquo;</a>
				</div>
			</div>
		</script>
        <?php
		}

	}

}

$f24_sc_dalectete = new f24_shortcode_dalectete();
$f24_sc_dalectete->init();
