<?php

/*
Představte si, že máte článek s fotogalerii, ze kterého potřebujete stáhnout všechny fotografie v plné kvalitě. Normálně to budete přes prohlížeč stahovat postupně a WordPress sám o sobě neumožňuje hromadné stahování.. tudíž tento nástroj tohle řeší, že člověk zadá URL článku a on vše stáhne a zabalí do jednoho zipu.
 */

require( 'wp-load.php' );

$postid = $_GET['postid'];
$post = get_page_by_title($postid, OBJECT, 'post');
$newgallery  = get_field('f24_gallery', $post->ID);
if($newgallery):
	foreach ($newgallery as $image) :
		$files[] = $image['sizes']['large'];
		$zdroj[] = '<p style="clear: both; overflow: hidden; margin-bottom: 10px;"><img src="'.$image['sizes']['gallery'].'" style="float:left; margin-right: 10px;">' . $image['caption'] . '</p>';
	endforeach;
endif;

if(isset($_POST['submit'])) {
	$zipname = $post->post_name.'.zip';
	$zip = new ZipArchive;
	$zip->open($zipname, ZipArchive::CREATE);
	foreach ($files as $file) {
		$download_file = file_get_contents($file);
		$zip->addFromString(basename($file), $download_file);
	}
	$zip->close();

	header('Content-Type: application/zip');
	header('Content-disposition: attachment; filename='.$zipname);
	header('Content-Length: ' . filesize($zipname));
	readfile($zipname);
	unlink($zipname);
}

?>

<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://www.w3schools.com/lib/w3.css">
	<title>Image Downloader</title>
</head>
<body>
<div class="w3-container" style="max-width:1200px; margin: 20px auto;">

	<form method="get" class="w3-container">
		<label><b>Titulek článku</b></label>
		<input id="postid" name="postid" class="w3-input w3-border" type="text" value="<?php echo $postid; ?>">
		<br/>
		<button class="w3-btn w3-blue">Otevřít</button>
	</form>

	<?php
		if($_GET['postid']) :
	?>

	<form method="post" class="w3-container">
		<input class="w3-button w3-red" type="submit" name="submit" value="Stáhnout fotky">
	</form>

	<?php
		endif;
	?>

	<div class="w3-container">
	<?php
		if($_GET['postid']) :
			if(f24_get_photo_info('gallery-author')) {
				echo '<p>Autor foto: '. f24_get_photo_info('gallery-author') . '</p>';
			} else {
				foreach ($zdroj as $zdroj) {
					echo $zdroj;
				}
			}
		endif;
	?>
	</div>

</div>
</body>
</html>
