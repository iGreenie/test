<?php 

// CRON (jeden soubor)
$xml = simplexml_load_file("https://www.volby.cz/pls/prez2018/vysledky");

$zeman = $xml->CR->KANDIDAT[6];
$drahos = $xml->CR->KANDIDAT[8];
$ucast = $xml->CR->UCAST[1];

$output = array();

$output['zeman'] = array(
	'jméno' => 'Miloš Zeman',
	'hlasy' => (integer)$zeman['HLASY_2KOLO'],
	'procento' => (float)$zeman['HLASY_PROC_2KOLO']
);

$output['drahos'] = array(
	'jméno' => 'Jiří Drahoš',
	'hlasy' => (integer)$drahos['HLASY_2KOLO'],
	'procento' => (float)$drahos['HLASY_PROC_2KOLO']
);

$output['ucast'] = array(
	'procento' => (float)$ucast['OKRSKY_ZPRAC_PROC']
);

$json = json_encode($output);
update_option( 'f24_volby_2018_prezident', $json );

// KONEC CRON 

// DALSI SOUBOR
$volby = get_option( 'f24_volby_2018_prezident' );
$volby = json_decode($volby);

$zeman = $volby->zeman;
$drahos = $volby->drahos;
$ucast = $volby->ucast;

$zeman->hlasy = number_format($zeman->hlasy, 0, '', ' ');
$drahos->hlasy = number_format($drahos->hlasy, 0, '', ' ');

?>

<div class="section-heading color-svobodne-forum hide-for-small-only" style="margin-top: 20px;">
	<h2>Výsledky voleb <span class="volbysecteno">| sečteno: <?php echo $ucast->procento; ?>%</h2>
</div>

<div class="section-heading color-svobodne-forum show-for-small-only" style="margin-top: 20px;">
	<h2>Výsledky voleb<span class="volbysecteno">sečteno: <?php echo $ucast->procento; ?>%</h2>
</div>

<div class="graf-balance" style="margin-top: 10px;">
	<div class="text">
		<span class="name zeman">Miloš Zeman</span>
		<span class="name drahos" style="float: right;">Jiří Drahoš</span>
	</div>
	<div class="visual">
		<i class="tick"></i>
		<div class="bar">
			<div class="bar-left">
				<i class="fill fill-red" style="width: <?php echo $zeman->procento; ?>%"></i>
			</div>
			<div class="bar-right">
				<i class="fill fill-blue" style="width: <?php echo $drahos->procento; ?>%;"></i>
			</div>
		</div>
	</div>
	<div class="text" style="padding-bottom: 10px;">
		<span class="votes zeman"><b><?php echo $zeman->procento; ?>% hlasů</b><span class="total"> | <?php echo $zeman->hlasy; ?></span></span>
		<span class="votes drahos"><span class="total"><?php echo $drahos->hlasy; ?> | </span><b><?php echo $drahos->procento; ?>% hlasů</b></span>
	</div>
	<div class="headshots">
		<img src="http://forum24.cz/wp-content/uploads/2018/01/volby_zeman2.jpg" class="zeman">
		<img src="http://forum24.cz/wp-content/uploads/2018/01/volby_drahos2.jpg" class="drahos">
	</div>
</div>
